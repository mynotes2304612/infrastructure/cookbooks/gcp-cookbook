#!/bin/bash
#
# This script install gcloud on Linux x86_64

### SCRIPT VARIABLES ###

INSTALL_DIR=/opt # this can require root permissions to write on it (So you must run this script as root)
TMP_DIR=/tmp

########################

pkg_uncompressed_dir='google-cloud-sdk'
pkg="google-cloud-cli-431.0.0-linux-x86_64.tar.gz"
pkg_url="https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/$pkg"
pkg_sha256="98f1d8d402acccb09cb0edbfe163ab4869bd79852f7378519784b2f89519c263"

echo "[*] Downloading $pkg"
curl --output-dir $TMP_DIR -O $pkg_url

TF=$TMP_DIR/$pkg.sha256sum
cat << EOF > $TF 
$pkg_sha256 $pkg
EOF

echo "[*] Checking integrity of file $pkg"
cd $TMP_DIR && sha256sum -c $TF

if [[ $? -ne 0 ]];then
  echo "[*] Signature of $pkg doesn't match with $pkg_sha256"
  exit 1
fi

tar -xzvf $TMP_DIR/$pkg -C $INSTALL_DIR

echo "[*] Installing gcloud"
sudo -u $USER bash "$INSTALL_DIR/$pkg_uncompressed_dir/install.sh" \
  --usage-reporting false \
  --path-update true \
  --command-completion true \
