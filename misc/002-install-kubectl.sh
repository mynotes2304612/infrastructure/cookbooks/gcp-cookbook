#!/bin/bash
#
# This script install kubectl on Linux x86_64

### SCRIPT VARIABLES ###

BASEDIR=/tmp

#########################


version=$(curl -L -s https://dl.k8s.io/release/stable.txt)
pkg=kubectl
pkg_url="https://dl.k8s.io/release/$version/bin/linux/amd64/$pkg"
pkg_sha256_url="https://dl.k8s.io/$version/bin/linux/amd64/$pkg.sha256"

curl --output-dir $BASEDIR -LO $pkg_url

echo "[*] Checking integrity of $pkg file" 
pkg_sha256=$(curl -s -L "https://dl.k8s.io/$version/bin/linux/amd64/$pkg.sha256") 
cat << EOF > $BASEDIR/$pkg.sha256
$pkg_sha256 $pkg
EOF
cd $BASEDIR && sha256sum -c $BASEDIR/$pkg.sha256

if [[ $? -ne 0 ]];then
  echo "[*] Signature of $pkg doesn't match with $pkg_sha256"
  exit 1
fi

echo "[*] Installing $pkg to /usr/local/bin"
sudo install -o root -g root -m 0755 $BASEDIR/$pkg /usr/local/bin/$pkg
